/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 University of Campinas (Unicamp)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


#include "ns3/lte-helper.h"
#include "ns3/epc-helper.h"
#include "ns3/core-module.h"
#include "ns3/wifi-module.h"
#include "ns3/network-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/internet-module.h"
#include "ns3/mobility-module.h"
#include "ns3/lte-module.h"
#include "ns3/applications-module.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/config-store.h"
#include <ns3/csma-module.h>
#include <ns3/ofswitch13-module.h>
#include <ns3/internet-apps-module.h>
//#include <ns3/lte-controller.h>
//#include <ns3/qos-controller.h>
#include <ns3/flow-monitor-helper.h>
#include <ns3/radio-bearer-stats-calculator.h>
#include <ns3/ipv4-flow-classifier.h>
#include <cstdlib>
#include <cmath>

using namespace ns3;
using namespace std;

/**
 * Several eNodeB,
 * attaches one UE per eNodeB
 * starts a flow for each UE to  and from a remote host.
 * starts another flow between each UE pair.
 */

NS_LOG_COMPONENT_DEFINE ("EpcFirstExample");

//apo generic
std::map<std::string, WifiPhyStandard> standard_map;


void ThroughputMonitor (FlowMonitorHelper *fmhelper, Ptr<FlowMonitor> flowMon)
	{
		std::map<FlowId, FlowMonitor::FlowStats> flowStats = flowMon->GetFlowStats();
		Ptr<Ipv4FlowClassifier> classing = DynamicCast<Ipv4FlowClassifier> (fmhelper->GetClassifier());
		for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator stats = flowStats.begin (); stats != flowStats.end (); ++stats)
		{
			Ipv4FlowClassifier::FiveTuple fiveTuple = classing->FindFlow (stats->first);
			std::cout<<"Flow ID			: " << stats->first <<" ; "<< fiveTuple.sourceAddress <<" -----> "<<fiveTuple.destinationAddress<<std::endl;
			std::cout<<"Tx Packets = " << stats->second.txPackets<<std::endl;
			std::cout<<"Rx Packets = " << stats->second.rxPackets<<std::endl;
			std::cout<<"Duration		: "<<stats->second.timeLastRxPacket.GetSeconds()-stats->second.timeFirstTxPacket.GetSeconds()<<std::endl;
			std::cout<<"Last Received Packet	: "<< stats->second.timeLastRxPacket.GetSeconds()<<" Seconds"<<std::endl;
			std::cout<<"Throughput: " << stats->second.rxBytes * 8.0 / (stats->second.timeLastRxPacket.GetSeconds()-stats->second.timeFirstTxPacket.GetSeconds())/1024  << " Mbps"<<std::endl;
			std::cout<<"---------------------------------------------------------------------------"<<std::endl;
		}
			Simulator::Schedule(Seconds(1),&ThroughputMonitor, fmhelper, flowMon);

	}

//apo generic 

/*****
Vector calcCirclePoint(float radius)
{
	float angle=rand()%360;

	float x = cos(angle)*radius;
	float y = sin(angle)*radius;
	std::cout<<"Random point is x:"<<x<<" y: "<<y<<endl;

	return Vector (x,y,0);
}
*****/

int main (int argc, char *argv[])
{

	uint16_t numberOfNodes = 5; //PAizei rolo sto packet loss
	double simTime = 10.1;
	double distance = 60.0; //meters
	double interPacketInterval = 100;

	//apo generic ***********
	uint32_t nWifi = 8;
	uint32_t channelWidth = 40; //MHz
	uint32_t channelNumber = 1;	//one channel
	bool useShortGuardInterval = false;
	bool useRts = false;
	bool udp = false;
	string standard = "802.11n-2.4GHz";

	//telos generic**************



	// Command line arguments
	CommandLine cmd;
	cmd.AddValue("numberOfNodes", "Number of eNodeBs + UE pairs", numberOfNodes);
	cmd.AddValue("simTime", "Total duration of the simulation [s])", simTime);
	cmd.AddValue("distance", "Distance between eNBs [m]", distance);
	cmd.AddValue("interPacketInterval", "Inter packet interval [ms])", interPacketInterval);
	
	// ^^^^^^^^^^^^generic ^^^^^^^^^^^
	cmd.AddValue("nWifi", "Number of stations", nWifi);
	cmd.AddValue("distance", "Distance in meters between the stations and the access point", distance);
	cmd.AddValue("useRts", "Enable/disable RTS/CTS", useRts);
	cmd.AddValue("channelWidth", "Channel width in MHz", channelWidth);
	cmd.AddValue("useShortGuardInterval", "Enable/disable short guard interval", useShortGuardInterval);
	cmd.AddValue("wifiStandard", "Set Wifi standard 802.11a/b/g/n/ac", standard);
	cmd.AddValue("channelNumber", "Channel numberl like 1,2,3... etc for 2.4GHz 36,40.44...etc
				 for 5GHz", channelNumber);

	cmd.AddValue("udp", "If true then udp will be used to send PacketSizeBinWidth
				  if false then tcp-ip", udp);


	////^^^^^^^generic^^^^^^^
	cmd.Parse(argc, argv);

	// Enable checksum computations (required by OFSwitch13 module)
	GlobalValue::Bind ("ChecksumEnabled", BooleanValue (true));

	FlowMonitorHelper fl;
  Ptr<FlowMonitor> monitor;

	// Create the switch node
	//Ptr<Node> switchNode = CreateObject<Node> ();

	//Create 3 switch nodes A,B,D
	NodeContainer switches;
    switches.Create (3);
	NetDeviceContainer switchPorts [4];


	

	// Use the CsmaHelper to connect host nodes to the switch node
	CsmaHelper csmaHelper;
	csmaHelper.SetChannelAttribute ("DataRate", DataRateValue (DataRate ("100Gb/s")));
	//csmaHelper.SetChannelAttribute("Mtu", UintegerValue (1500));
	csmaHelper.SetChannelAttribute ("Delay", TimeValue (Seconds (0.010)));

	Ptr<LteHelper> lteHelper = CreateObject<LteHelper> ();
	Ptr<PointToPointEpcHelper>  epcHelper = CreateObject<PointToPointEpcHelper> ();
	lteHelper->SetEpcHelper (epcHelper);

	ConfigStore inputConfig;
	inputConfig.ConfigureDefaults();

	// parse again so you can override default values from the command line
	cmd.Parse(argc, argv);

	Ptr<Node> pgw = epcHelper->GetPgwNode ();

	// Create a single RemoteHost
	NodeContainer remoteHostContainer;
	remoteHostContainer.Create (1);
	Ptr<Node> remoteHost = remoteHostContainer.Get (0);

	NetDeviceContainer hostDevices;
	NetDeviceContainer pairDevs;

	//NetDeviceContainer switchPorts;
	NodeContainer pair;
	NodeContainer hosts;
	hosts.Add(pgw);
	hosts.Add(remoteHost);

		// ~~~~~~~~~~~~ generic~~~~~~~~~
	NodeContainer wifiStaNodes;
	wifiStaNodes.Create(nWifi);
	NodeContainer wifiApNode;
	wifiApNode.Create(2);
		

	//----------SETTING UP STANDARDS MAP -----------
	standard_map["802.11a"] = WIFI_PHY_STANDARD_80211a;
  	standard_map["802.11b"] = WIFI_PHY_STANDARD_80211b;
  	standard_map["802.11g"] = WIFI_PHY_STANDARD_80211g;
  	standard_map["802.11n-2.4GHz"] = WIFI_PHY_STANDARD_80211n_2_4GHZ;
  	standard_map["802.11n-5GHz"] = WIFI_PHY_STANDARD_80211n_5GHZ;
  	standard_map["802.11ac"] = WIFI_PHY_STANDARD_80211ac;
  	// ------------SETTING UP STANDARDS MAP----------------//

  
  	YansWifiChannelHelper channel ;
	//YansWifiChannelHelper channel = YansWifiChannelHelper::Default ();
  	YansWifiPhyHelper phy = YansWifiPhyHelper::Default ();

  	// ------------SET PROPAGATION DELAY----------------
  	// to set propagation loss model the channel helper should not be default.
  	
  	channel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  	channel.AddPropagationLoss ("ns3::FriisPropagationLossModel","Frequency",DoubleValue(2.412e9));
  	
  	// ------------PROPAGATION DELAY SET ----------------//

  	phy.Set("ChannelNumber", UintegerValue(channelNumber));
  	phy.Set("ShortGuardEnabled", BooleanValue(useShortGuardInterval));
  	//Set guard interval
  	phy.SetChanel(channel.Create());

  	phy.Set ("TxGain", DoubleValue (0));
  	phy.Set ("RxGain", DoubleValue(0));
  	phy.Set ("TxPowerStart", DoubleValue (0.0));
  	phy.Set ("TxPowerEnd", DoubleValue (0.0));

  	WifiMacHelper mac;
  	WifiHelper wifi;
  	wifi.SetStandard (standard_map[standard]);

  	uint32_t rtsThreshold = 65535;
  	//wifi.SetRemoteStationManager ("ns3::MinstrelHtWifiManager", "RtsCtsThreshold", UintegerValue (rtsThreshold));
  	//MinstrelHtWifiManager
  	
  	wifi.SetRemoteStationManager ("ns3::IdealWifiManager", "RtsCtsThreshold", UintegerValue (rtsThreshold)); //MinstrelHtWifiManager


	//~~~~~~~~generic~~~~~~~~~~~~~


	//Connect pgw(host0) ------ switch A
	pair=NodeContainer(hosts.Get(0), switches.Get(0));
	pairDevs=csmaHelper.Install(pair);
	hostDevices.Add(pairDevs.Get(0));
	switchPorts[0].Add(pairDevs.Get(1));

	//Connect remoteHost [C] (host1) ------ switch D
	pair=NodeContainer(hosts.Get(1), switches.Get(1));
	pairDevs=csmaHelper.Install(pair);
	hostDevices.Add(pairDevs.Get(0));
	switchPorts[1].Add(pairDevs.Get(1));


	//Connect remoteHost -------switch B
	pair=NodeContainer(hosts.Get(1), switches.Get(2));
	pairDevs=csmaHelper.Install(pair);
	hostDevices.Add(pairDevs.Get(0));
	switchPorts[1].Add(pairDevs.Get(1));


	//Connect the switches
	//A-----D
	pair=NodeContainer(switches.Get(0),switches.Get(1));
	pairDevs=csmaHelper.Install(pair);
	switchPorts[0].Add(pairDevs.Get(0));
	switchPorts[1].Add(pairDevs.Get(1));




	//	A--------B
	pair=NodeContainer(switches.Get(0), switches.Get(2));
	pairDevs=csmaHelper.Install(pair);
	switchPorts[0].Add(pairDevs.Get(0));
	switchPorts[2].Add(pairDevs.Get(1));



	//%%%%%%%%%% generic %%%%%%%%%%%%%%%
	Ssid ssid = Ssid ("Placebo");
	mac.SetType ("ns3::StaWifiMac", "Ssid". SsidValue (ssid));

	NetDeviceContainer staDevices;
	staDevices= wifi.Install (phy, mac, wifiStaNodes);

	mac.SetType ("ns3::ApWifiMac", "Ssid", SsidValue(ssid));

	NetDeviceContainer apDevice;
	apDevice= wifi.Install (phy, mac, wifiApNode);

	Mac48Address ap_addr_1="AA:AA:27:26:4D:11";
	apDevice.Get(0)->SetAddress(ap_addr_1);

	Mac48Address ap_addr_2="AA:AA:27:26:4D:12";
	apDevice.Get(1)->SetAddress(ap_addr_2);


	//Set channel width  
	Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ChannelWidth",  UintegerValue(channelWidth));
  	//Config::Set ("/NodeList/*/DeviceList/*/$ns3::WifiNetDevice/Phy/ShortGuardEnabled",  BooleanValue(useShortGuardInterval));

	//%%%%%%%%%%%%%generic%%%%%%%%%%%%%


	//GENERIC

	//mobility
	MobilityHelper mobilityWifi;
	Ptr<ListPositionAllocator> positionAlloc= CreateObject<listPositionAllocator> ();
	positionAlloc->Add (Vector (0.0, 0.0, 0.0));
	for (uint32_t i=0; i<nWifi; i++){
		positionAlloc->Add (calcCirclePoint(distance));
	}

	mobilityWifi.SetPositionAllocator (positionAlloc);
	mobilityWifi.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
	mobilityWifi.Install (wifiApNode);
	mobilityWifi.Install (wifiStaNodes);



	//Internet Stack
	InternetStackHelper stack;
	stack.Install (wifiApNode);
	stack.Install (wifiStaNodes);
	Ipv4AddressHelper address;

	address.SetBase ("192.168.1.0", "255.255.255.0");
	Ipv4InterfaceContainer staNodeInterfaces, apNodeInterface;

	staNodeInterfaces = address.Assign (staDevices);
	apNodeInterface = address.Assign (apDevice);


	//Setting applications
	ApplicationContainer sourceApplications, sinkApplications;

	std::string udp_tcp_on_off= "";
	std::string udp_tcp_packetsink= "";
	uint32_t portNumber = 9;
	Ipv4GlobalRoutingHelper::PopoulateRoutingTables ();

	for(uint8_t index =0; index < 4; ++index)
	{
		auto ipv4 = wifiApNode.Get (0)->GetObject<Ipv4> ();
		const auto address = ipv4->GetAddress (1,0).GetLocal ();
		InetSocketAddress sinkSocket (address, portNumber++);
		if(udp){
			udp_tcp_on_off = "ns3::UdpSocketFactory";
			udp_tcp_packetsink = "ns3::UdpSocketFactory";
		}
		else{
			udp_tcp_on_off = "ns3::TcpSocketFactory";
			udp_tcp_packetsink = "ns3::TcpSocketFactory";
		}

		OnOffHelper OnOffHelper (udp_tcp_on_off, sinkSocket);
		PacketSinkHelper PacketSinkHelper (udp_tcp_packetsink, sinkSocket);
		onOffHelper.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));

		onOffHelper.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));

		onOffHelper.SetAttribute ("DataRate", StringValue ("100Mbps"));
		//application layer data rate

		onOffHelper.SetAttribute ("PacketSize", UintegerValue (1472));
		//bytes

		sourceApplications.Add (onOffHelper.Install(wifiStaNodes.Get (index)));

		sinkApplications.Add (packetSinkHelper.Install(wifiApNode.Get (0)));
	}

	for(uint8_t index =4; index < 8; ++index)
	{
		auto ipv4 = wifiApNode.Get (1)->GetObject<Ipv4> ();
		const auto address = ipv4->GetAddress (1,0).GetLocal ();
		InetSocketAddress sinkSocket (address, portNumber++);
		
		if(udp){
			udp_tcp_on_off = "ns3::UdpSocketFactory";
			udp_tcp_packetsink = "ns3::UdpSocketFactory";
		}
		else{
			udp_tcp_on_off = "ns3::TcpSocketFactory";
			udp_tcp_packetsink = "ns3::TcpSocketFactory";
		}

		OnOffHelper OnOffHelper (udp_tcp_on_off, sinkSocket);
		PacketSinkHelper PacketSinkHelper (udp_tcp_packetsink, sinkSocket);
		onOffHelper.SetAttribute ("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=1]"));
		onOffHelper.SetAttribute ("OffTime", StringValue ("ns3::ConstantRandomVariable[Constant=0]"));
		onOffHelper.SetAttribute ("DataRate", StringValue ("100Mbps"));
		//application layer data rate

		onOffHelper.SetAttribute ("PacketSize", UintegerValue (1472));
		//bytes

		sourceApplications.Add (onOffHelper.Install(wifiStaNodes.Get (index)));
		sinkApplications.Add (packetSinkHelper.Install(wifiApNode.Get (1)));
	}

	sinkApplications.Start (Seconds (0.0));
	sinkApplications.Stop (Seconds (simulationTime + 1));
	sourceApplications.Start (Seconds (1.0));
	sourceApplications.Stop (Seconds (simulationTime + 1));


	//create pcap files





	//GENERIC


	// Create the controller node
	Ptr<Node> controllerNode = CreateObject<Node> ();

	// Configure the OpenFlow network domain
	/*Ptr<OFSwitch13InternalHelper> of13Helper = CreateObject<OFSwitch13InternalHelper> ();
	Ptr<OFSwitch13LearningController> learnCtrl = CreateObject<OFSwitch13LearningController> ();
	of13Helper->InstallController (controllerNode, learnCtrl);*/

	Ptr<OFSwitch13InternalHelper> of13Helper = CreateObject<OFSwitch13InternalHelper> ();
  	of13Helper->InstallController (controllerNode);

	of13Helper->InstallSwitch (switches.Get(0), switchPorts[0]);
	of13Helper->InstallSwitch (switches.Get (1), switchPorts[1]);
	of13Helper->InstallSwitch (switches.Get (2), switchPorts[2]);
	of13Helper->CreateOpenFlowChannels ();


	InternetStackHelper internet;
	internet.Install (remoteHostContainer);

	Ipv4AddressHelper ipv4h;
	ipv4h.SetBase ("1.0.0.0", "255.0.0.0");
	Ipv4InterfaceContainer internetIpIfaces = ipv4h.Assign (hostDevices);
	// interface 0 is localhost, 1 is the p2p device
	Ipv4Address remoteHostAddr = internetIpIfaces.GetAddress (1);

	Ipv4StaticRoutingHelper ipv4RoutingHelper;
	Ptr<Ipv4StaticRouting> remoteHostStaticRouting = ipv4RoutingHelper.GetStaticRouting (remoteHost->GetObject<Ipv4> ());
	remoteHostStaticRouting->AddNetworkRouteTo (Ipv4Address ("6.0.0.0"), Ipv4Mask ("255.0.0.0"), 1);

	NodeContainer ueNodes;
	NodeContainer enbNodes;
	enbNodes.Create(1);
	ueNodes.Create(numberOfNodes);

	
	// Install Mobility Model
	Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
	for (uint16_t i = 0; i < numberOfNodes; i++)
	{
	  positionAlloc->Add (Vector(distance * i, 0, 0));
	}
	MobilityHelper mobility;
	mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	mobility.SetPositionAllocator(positionAlloc);
	mobility.Install(ueNodes);
	positionAlloc->Add (Vector(0, 0, 0));
	mobility.SetPositionAllocator(positionAlloc);
	mobility.Install(enbNodes);

	lteHelper->SetSchedulerType ("ns3::CqaFfMacScheduler");

	// Install LTE Devices to the nodes
	NetDeviceContainer enbLteDevs = lteHelper->InstallEnbDevice (enbNodes);
	NetDeviceContainer ueLteDevs = lteHelper->InstallUeDevice (ueNodes);

	// Install the IP stack on the UEs
	internet.Install (ueNodes);
	Ipv4InterfaceContainer ueIpIface;
	ueIpIface = epcHelper->AssignUeIpv4Address (NetDeviceContainer (ueLteDevs));
	// Assign IP address to UEs, and install applications
	for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
	{
	  Ptr<Node> ueNode = ueNodes.Get (u);
	  // Set the default gateway for the UE
	  Ptr<Ipv4StaticRouting> ueStaticRouting = ipv4RoutingHelper.GetStaticRouting (ueNode->GetObject<Ipv4> ());
	  ueStaticRouting->SetDefaultRoute (epcHelper->GetUeDefaultGatewayAddress (), 1);
	}

	// Attach one UE per eNodeB
	for (uint16_t i = 0; i < numberOfNodes; i++)
	  {
	    lteHelper->Attach (ueLteDevs.Get(i), enbLteDevs.Get(0));
	    // side effect: the default EPS bearer will be activated
	  }

for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
    {
      Ptr<NetDevice> ueDevice = ueLteDevs.Get (u);

      enum EpsBearer::Qci q;

      switch (u+1)
      {
         case 1:
            		q = EpsBearer::GBR_CONV_VOICE;
            			break;
         case 2:
        			q = EpsBearer::GBR_CONV_VIDEO;
            			break;
         case 3:
        			q = EpsBearer::GBR_GAMING;
            			break;
         case 4:
        	 		q = EpsBearer::GBR_NON_CONV_VIDEO;
            			break;
         case 5:
			 		q = EpsBearer:: NGBR_IMS;
			 	 		break;
         case 6:
			 		q = EpsBearer::NGBR_VIDEO_TCP_OPERATOR;
			 	 	 	break;
         case 7:
			 		q = EpsBearer::NGBR_VOICE_VIDEO_GAMING;
			 			break;
         case 8:
			 		q = EpsBearer::NGBR_VIDEO_TCP_PREMIUM;
			 			break;
         case 9:
			 		q = EpsBearer::NGBR_VIDEO_TCP_DEFAULT;
			 			break;
         default:
             		q = EpsBearer::NGBR_VIDEO_TCP_DEFAULT;
      }

      EpsBearer bearer (q);

      lteHelper->ActivateDedicatedEpsBearer (ueDevice, bearer, EpcTft::Default ());
    }


	// Install and start applications on UEs and remote host
	uint16_t ulport = 8000;
	uint16_t dlport = 3000;
	ApplicationContainer clientApps;
	ApplicationContainer serverApps;
	for (uint32_t u = 0; u < ueNodes.GetN (); ++u)
	{


	  //DOWNLINK SERVER->UE APP
	  PacketSinkHelper dlClientPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (ueIpIface.GetAddress (u), dlport));
	  clientApps.Add (dlClientPacketSinkHelper.Install (ueNodes.Get(u)));

	  UdpClientHelper ulServer (ueIpIface.GetAddress (u), ulport);
	  ulServer.SetAttribute ("Interval", TimeValue (MilliSeconds(interPacketInterval)));
	  ulServer.SetAttribute ("MaxPackets", UintegerValue(1000000));
	  ulServer.SetAttribute ("PacketSize", UintegerValue(1024));
	  serverApps.Add (ulServer.Install (remoteHostContainer));
		cout<<"Downlink DONE"<<endl;


	  /*UPLINK UE->SERVER APP*/
	  PacketSinkHelper dlServerPacketSinkHelper ("ns3::UdpSocketFactory", InetSocketAddress (remoteHostAddr, dlport));
	  serverApps.Add (dlServerPacketSinkHelper.Install (remoteHostContainer));

	  UdpClientHelper ulClient (remoteHostAddr, ulport);
	  ulClient.SetAttribute ("Interval", TimeValue (MilliSeconds(interPacketInterval)));
	  ulClient.SetAttribute ("MaxPackets", UintegerValue(1000000));
	  ulClient.SetAttribute ("PacketSize", UintegerValue(1024));
	  clientApps.Add (ulClient.Install (ueNodes.Get(u)));
		cout<<"Uplink DONE"<<endl;


	  dlport++;
	  ulport++;

	}
	serverApps.Start (Seconds (0.01));
	clientApps.Start (Seconds (0.01));


	monitor = fl.Install(ueNodes);
  monitor = fl.Install(remoteHost);

   monitor->SetAttribute ("DelayBinWidth", DoubleValue(0.001));
   monitor->SetAttribute ("JitterBinWidth", DoubleValue(0.001));
   monitor->SetAttribute ("PacketSizeBinWidth", DoubleValue(20));

   Simulator::Schedule(Seconds(3),&ThroughputMonitor,&fl, monitor);

	//lteHelper->EnableTraces ();
	// Uncomment to enable PCAP tracing
	//p2ph.EnablePcapAll("lena-epc-first");


	Simulator::Stop(Seconds(simTime));
	Simulator::Run();

	ThroughputMonitor(&fl, monitor);

	//monitor->SerializeToXmlFile ("results.xml" , true, true );

	/*GtkConfigStore config;
	config.ConfigureAttributes();*/

	Simulator::Destroy();
	return 0;

}

